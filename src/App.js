import "bootstrap/dist/css/bootstrap.min.css";
import { Route, Routes } from "react-router-dom";
import './App.css';
import Sidebar from "./components/sidebar/Sidebar.js";
import Content from "./components/content/Content.js";
import Footer from "./components/footer/Footer.js";
import Products from "./components/content/products/Products.js";
import Detail from "./components/content/detail/Detail.js";
import { Grid } from "@mui/material";

function App() {
  return (
    <div className="App">
      <Grid container style={{height: "auto"}}>
        <Sidebar />
        <Routes>
          <Route path="/" element={<Content />}></Route>
          <Route path="/products" element={<Products />}></Route>
          <Route path="/detail" element={<Detail />}></Route>
          <Route path="*" element={<Content />}></Route>
        </Routes>
      </Grid>
      <Footer />
    </div>
  );
};

export default App;
