import React from "react";
import { Col, Nav, NavItem, NavLink, Row } from "reactstrap";

const NavigationSideBar = () => {
return (
    <>
        <Row style={{marginTop: "100px"}}>
                <Col xl={12}>
                    <Nav vertical>
                        <NavItem className="main-sidebar-item nav-btn">
                            <NavLink style={{fontWeight: "600", fontSize: "17px"}} className="main-sidebar-a mx-0 px-0" href="/">HOME</NavLink>
                        </NavItem>
                        <NavItem className="main-sidebar-item nav-btn">
                            <NavLink style={{fontWeight: "600", fontSize: "17px"}} className="main-sidebar-a mx-0 px-0" href="/products">SHOP</NavLink>
                        </NavItem>
                        <NavItem className="main-sidebar-item nav-btn">
                            <NavLink style={{fontWeight: "600", fontSize: "17px"}} className="main-sidebar-a mx-0 px-0" href="/detail">PRODUCT</NavLink>
                        </NavItem>
                        <NavItem className="main-sidebar-item nav-btn">
                            <NavLink style={{fontWeight: "600", fontSize: "17px"}} className="main-sidebar-a mx-0 px-0" href="#">CART</NavLink>
                        </NavItem>
                        <NavItem className="main-sidebar-item nav-btn">
                            <NavLink style={{fontWeight: "600", fontSize: "17px"}} className="main-sidebar-a mx-0 px-0" href="#">CHECKOUT</NavLink>
                        </NavItem>
                    </Nav>

                </Col>
            </Row>
    </>
);
};

export default NavigationSideBar;