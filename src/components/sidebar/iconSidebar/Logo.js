import logoImage from '../../../assets/images/icon/logo.png';

function Logo() {
    return (
        <img className='logo-image' src={logoImage}></img>
    );
};

export default Logo;