import {
    GET_ALL_PRODUCTS_REQUEST,
    GET_ALL_PRODUCTS_SUCCESS,
    GET_ALL_PRODUCTS_ERROR,
    GET_ALL_PRODUCTS_WITH_CONDITION_REQUEST,
    GET_ALL_PRODUCTS_WITH_CONDITION_SUCCESS,
    GET_ALL_PRODUCTS_WITH_CONDITION_ERROR
} from "./productsActionTypes.js";

const getAllProductsRequest = () => ({
    type: GET_ALL_PRODUCTS_REQUEST
});

const getAllProductsSuccess = (allProducts) => ({
    type: GET_ALL_PRODUCTS_SUCCESS,
    payload: allProducts
});

const getAllProductsError = (errorMessages) => ({
    type: GET_ALL_PRODUCTS_ERROR,
    payload: errorMessages
});

const getAllProductsWithConditionRequest = () => ({
    type: GET_ALL_PRODUCTS_WITH_CONDITION_REQUEST
});

const getAllProductsWithConditionSuccess = (allProducts) => ({
    type: GET_ALL_PRODUCTS_WITH_CONDITION_SUCCESS,
    payload: allProducts
});

const getAllProductsWithConditionError = (errorMessages) => ({
    type: GET_ALL_PRODUCTS_WITH_CONDITION_ERROR,
    payload: errorMessages
});

export {
    getAllProductsRequest,
    getAllProductsSuccess,
    getAllProductsError,
    getAllProductsWithConditionRequest,
    getAllProductsWithConditionSuccess,
    getAllProductsWithConditionError
};