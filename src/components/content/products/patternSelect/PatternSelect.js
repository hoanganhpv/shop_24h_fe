import { Grid } from "@mui/material";
import Categories from "./categories/Categories.js";
import Brands from "./brands/Brands.js";
import Colors from "./colors/Colors.js";
import Prices from "./prices/Price.js";

const PatternSelect = () => {
    return (
        <>
            <Grid item xl={3} lg={3} md={3} sm={3} xs={3} style={{ backgroundColor: "#F4F7FB", padding: "2%", margin: "0", height: "auto", minWidth: "190px" }}>
                <Grid container style={{ margin: "0px", padding: "0px", height: "auto" }}>
                    <Grid item xl={12} lg={12} md={12} sm={12} xs={12} style={{ margin: "80px 0px 70px 0px", padding: "0px" }} justifyContent={"center"}>
                        <Categories />
                    </Grid>
                    <Grid item xl={12} lg={12} md={12} sm={12} xs={12} style={{ margin: "0px 0px 70px", padding: "0px" }} justifyContent={"center"}>
                        <Brands />
                    </Grid>
                    <Grid item xl={12} lg={12} md={12} sm={12} xs={12} style={{ margin: "0px 0px 70px", padding: "0px" }} justifyContent={"center"}>
                        <Colors />
                    </Grid>
                    <Grid item xl={12} lg={12} md={12} sm={12} xs={12} style={{ margin: "0px 0px 70px", padding: "0px" }} justifyContent={"center"}>
                        <Prices />
                    </Grid>
                </Grid>
            </Grid>
        </>
    );
};

export default PatternSelect	