import { Grid } from "@mui/material";
import * as React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import StarIcon from '@mui/icons-material/Star';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import Button from '@mui/material/Button';
import ButtonGroup from '@mui/material/ButtonGroup';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import ClickAwayListener from '@mui/material/ClickAwayListener';
import Grow from '@mui/material/Grow';
import Paper from '@mui/material/Paper';
import Popper from '@mui/material/Popper';
import MenuItem from '@mui/material/MenuItem';
import MenuList from '@mui/material/MenuList';
import Pagination from '@mui/material/Pagination';
import Stack from '@mui/material/Stack';
import { useDispatch, useSelector } from "react-redux";
import {
    getAllProductsRequest,
    getAllProductsSuccess,
    getAllProductsError,
    getAllProductsWithConditionRequest,
    getAllProductsWithConditionSuccess,
    getAllProductsWithConditionError
} from "../action/productActionCreator";

const options = ['Date', 'Newest', 'Popular'];
const optionsPage = ['4', '6', '8', '10'];

const ProductList = () => {
    const dispatch = useDispatch();
    const [productsShow, setProductsShow] = React.useState(null);
    const { allProducts } = useSelector(dataFromReducer => dataFromReducer.products);


    const [limit, setLimit] = React.useState(4);
    const [page, setPage] = React.useState(1);
    const [totalPage, setTotalPage] = React.useState(0);
    const [startPage, setStartPage] = React.useState(1);
    const [endPage, setEndPage] = React.useState(1);
    const [pagePerPage, setpagePerPage] = React.useState(1);

    const fetchProductCondition = () => {
        return async () => {
            await new Promise(resolve => {
                dispatch(getAllProductsWithConditionRequest());
                resolve();
            });
            try {
                const fetchResult = await fetch(`http://localhost:8000/condition/?page=${page}&limit=${limit}`);
                const fetchJsonData = await fetchResult.json();
                await setTotalPage(fetchJsonData.totalPages);
                setStartPage((page - 1) * limit + 1);
                setEndPage(page * limit);
                setpagePerPage(12);
                const showData = await fetchJsonData.products;
                await dispatch(getAllProductsWithConditionSuccess(showData));
            } catch (err) {
                console.error(err.message);
                dispatch(getAllProductsWithConditionError(err.message));
            };
        };
    };

    React.useEffect(() => {
        fetchProductCondition()();
    }, [page, limit]);

    React.useEffect(() => {
        if (allProducts) {
            if (allProducts.length > 6) {
                let temp = [];
                for (let i = 0; i < allProducts.length; i++) {
                    temp.push(allProducts[i]);
                };
                setProductsShow(temp);
            } else {
                setProductsShow(allProducts);
            };
        };
    }, [allProducts]);


    const [openType, setOpenType] = React.useState(false);
    const anchorRef = React.useRef(null);
    const [selectedIndex, setSelectedIndex] = React.useState(1);

    const handleMenuItemClickType = (event, index) => {
        setSelectedIndex(index);
        setOpenType(false);
    };

    const handleToggleType = () => {
        setOpenType((prevOpen) => !prevOpen);
    };

    const handleCloseType = (event) => {
        if (anchorRef.current && anchorRef.current.contains(event.target)) {
            return;
        }

        setOpenType(false);
    };

    const [openPage, setOpenPage] = React.useState(false);
    const anchorRefPage = React.useRef(null);
    const [selectedIndexPage, setSelectedIndexPage] = React.useState(1);

    const handleMenuItemClickPage = (event, index) => {
        setSelectedIndexPage(index);
        setOpenPage(false);
        console.log(index);
        switch(index) {
            case 0: {
                setLimit(4);
                break;
            }
            case 1: {
                setLimit(6);
                break;
            }
            case 2: {
                setLimit(8);
                break;
            }
            case 3: {
                setLimit(10);
                break;
            }
        }
    };

    const handleTogglePage = () => {
        setOpenPage((prevOpen) => !prevOpen);
    };

    const handleClosePage = (event) => {
        if (anchorRefPage.current && anchorRefPage.current.contains(event.target)) {
            return;
        }

        setOpenPage(false);
    };

    return (
        <Grid style={{ padding: "0% 2% 0% 2%" }} item xl={9} lg={9} md={9} sm={9} xs={9}>
            {/* TOP - head */}
            <Grid item style={{ height: "8%", margin: "2%", maxHeight: "200px" }}>
                <Grid container style={{ position: "relative", height: "100%" }}>
                    <Grid item style={{ position: "absolute", bottom: "0", left: "0", width: "30%" }}>
                        <Typography style={{ fontWeight: "600" }}>SHOWING {startPage}-{endPage} OF {pagePerPage}</Typography>
                    </Grid>
                    <Grid item style={{ position: "absolute", bottom: "0", right: "0", width: "60%" }}>
                        <Grid container style={{ height: "100%", width: "100%" }} spacing={1}>
                            <Grid item xl={6} lg={6} md={6} sm={6} xs={6} style={{ paddingRight: "5px", height: "40px", paddingTop: "0px", paddingLeft: "0px" }}>
                                <Grid container style={{ backgroundColor: "#F5F7FA", height: "100%" }}>
                                    <Grid item xl={4} lg={4} md={4} sm={4} xs={4} style={{ height: "100%" }}>
                                        <Typography style={{ lineHeight: "40px", fontSize: "13px", paddingLeft: "10px", color: "#9495AE", fontWeight: "600" }}>Sort&nbsp;by</Typography>
                                    </Grid>
                                    <Grid item xl={8} lg={8} md={8} sm={8} xs={8} style={{ padding: "0px" }}>
                                        <ButtonGroup variant="contained" ref={anchorRef} aria-label="split button" style={{ height: "100%", width: "100%", backgroundColor: "#F5F7FA", boxShadow: "none", padding: "0", margin: "0" }} sx={{ borderRadius: 0 }}>
                                            <Button style={{ textTransform: "none", width: "80%", backgroundColor: "#F5F7FA", boxShadow: "none", borderColor: "#fff", color: "#212428" }} sx={{ borderRadius: 0 }}>{options[selectedIndex]}</Button>
                                            <Button
                                                size="small"
                                                aria-controls={openType ? 'split-button-menu' : undefined}
                                                aria-expanded={openType ? 'true' : undefined}
                                                aria-label="select merge strategy"
                                                aria-haspopup="menu"
                                                onClick={handleToggleType}
                                                sx={{ borderRadius: 0 }}
                                                style={{ backgroundColor: "#F5F7FA", boxShadow: "none", color: "#9495AE" }}
                                            >
                                                <ArrowDropDownIcon />
                                            </Button>
                                        </ButtonGroup>
                                        <Popper
                                            sx={{
                                                zIndex: 1,
                                            }}
                                            open={openType}
                                            anchorEl={anchorRef.current}
                                            role={undefined}
                                            transition
                                            disablePortal
                                        >
                                            {({ TransitionProps, placement }) => (
                                                <Grow
                                                    {...TransitionProps}
                                                    style={{
                                                        transformOrigin:
                                                            placement === 'bottom' ? 'center top' : 'center bottom',
                                                    }}
                                                >
                                                    <Paper>
                                                        <ClickAwayListener onClickAway={handleCloseType}>
                                                            <MenuList id="split-button-menu" autoFocusItem>
                                                                {options.map((option, index) => (
                                                                    <MenuItem
                                                                        key={option}
                                                                        // disabled={index === 2}
                                                                        selected={index === selectedIndex}
                                                                        onClick={(event) => handleMenuItemClickType(event, index)}
                                                                    >
                                                                        {option}
                                                                    </MenuItem>
                                                                ))}
                                                            </MenuList>
                                                        </ClickAwayListener>
                                                    </Paper>
                                                </Grow>
                                            )}
                                        </Popper>
                                    </Grid>
                                </Grid>
                            </Grid>
                            <Grid item xl={6} lg={6} md={6} sm={6} xs={6} style={{ paddingLeft: "5px", height: "40px", paddingTop: "0px", paddingLeft: "0px" }}>
                                <Grid container style={{ backgroundColor: "#F5F7FA", height: "100%" }}>
                                    <Grid item xl={4} lg={4} md={4} sm={4} xs={4} style={{ height: "100%" }}>
                                        <Typography style={{ lineHeight: "40px", fontSize: "13px", paddingLeft: "10px", color: "#9495AE", fontWeight: "600" }}>View</Typography>
                                    </Grid>
                                    <Grid item xl={8} lg={8} md={8} sm={8} xs={8} style={{ padding: "0px" }}>
                                        <ButtonGroup variant="contained" ref={anchorRefPage} aria-label="split button" style={{ height: "100%", width: "100%", backgroundColor: "#F5F7FA", boxShadow: "none", padding: "0", margin: "0" }} sx={{ borderRadius: 0 }}>
                                            <Button style={{ textTransform: "none", width: "80%", backgroundColor: "#F5F7FA", boxShadow: "none", borderColor: "#fff", color: "#212428" }} sx={{ borderRadius: 0 }}>{optionsPage[selectedIndexPage]}</Button>
                                            <Button
                                                size="small"
                                                aria-controls={openPage ? 'split-button-menu' : undefined}
                                                aria-expanded={openPage ? 'true' : undefined}
                                                aria-label="select merge strategy"
                                                aria-haspopup="menu"
                                                onClick={handleTogglePage}
                                                sx={{ borderRadius: 0 }}
                                                style={{ backgroundColor: "#F5F7FA", boxShadow: "none", color: "#9495AE" }}
                                            >
                                                <ArrowDropDownIcon />
                                            </Button>
                                        </ButtonGroup>
                                        <Popper
                                            sx={{
                                                zIndex: 1,
                                            }}
                                            open={openPage}
                                            anchorEl={anchorRefPage.current}
                                            role={undefined}
                                            transition
                                            disablePortal
                                        >
                                            {({ TransitionProps, placement }) => (
                                                <Grow
                                                    {...TransitionProps}
                                                    style={{
                                                        transformOrigin:
                                                            placement === 'bottom' ? 'center top' : 'center bottom',
                                                    }}
                                                >
                                                    <Paper>
                                                        <ClickAwayListener onClickAway={handleClosePage}>
                                                            <MenuList id="split-button-menu" autoFocusItem>
                                                                {optionsPage.map((option, index) => (
                                                                    <MenuItem
                                                                        key={option}
                                                                        // disabled={index === 2}
                                                                        selected={index === selectedIndexPage}
                                                                        onClick={(event) => handleMenuItemClickPage(event, index)}
                                                                    >
                                                                        {option}
                                                                    </MenuItem>
                                                                ))}
                                                            </MenuList>
                                                        </ClickAwayListener>
                                                    </Paper>
                                                </Grow>
                                            )}
                                        </Popper>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
            {/* BODY */}
            <Grid item style={{ height: "auto" }}>
                <Grid container style={{ paddingBottom: "300px" }}>
                    {productsShow ? productsShow.map((product, index) => {
                        return <Grid key={index} justifyContent={"center"} item xl={6} lg={6} md={6} sm={12} xs={12} style={{ padding: "2%" }}>
                            <Card style={{ margin: "0px auto", boxShadow: "none" }} sx={{ maxWidth: "100%" }}>
                                <CardMedia
                                    component="img"
                                    height="100%"
                                    width="100%"
                                    image={require(`../../../../assets/images/products/${product.imageUrl}.jpg`)}
                                    alt="green iguana"
                                />
                                <CardContent style={{ width: "100%", padding: "5% 0 2%" }}>
                                    <Grid justifyContent={"space-between"} style={{ width: "100%", padding: "0" }} container>
                                        <Grid item xl={3} lg={3} md={3} sm={3} xs={3} >
                                            <Grid container style={{ borderTop: "3px solid #FAB610" }}>
                                                <Grid item xl={12} lg={12} md={12} sm={12} xs={12} style={{ marginTop: "5%" }}>
                                                    <Typography style={{ color: "#FAB610", fontSize: "23px", fontWeight: "600", lineHeight: "30px" }}>${product.buyPrice}</Typography>
                                                </Grid>
                                                <Grid item xl={12} lg={12} md={12} sm={12} xs={12}>
                                                    <Typography style={{ color: "#24252B", fontSize: "15px", fontWeight: "600", lineHeight: "30px" }}>{product.name}</Typography>
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                        <Grid item xl={3} lg={3} md={3} sm={3} xs={3}>
                                            <Grid container style={{ margin: "auto", height: "100%" }}>
                                                <Grid item xl={12} lg={12} md={12} sm={12} xs={12} style={{ height: "40%", padding: "0", margin: "auto 0px 0px" }}>
                                                    <div style={{ marginRight: "0px", marginLeft: "auto", display: "flex", justifyContent: "flex-end", padding: "0px", height: "100%" }}>
                                                        <StarIcon style={{ fontSize: "13px", color: "#FAB610", margin: "auto 0px" }} />
                                                        <StarIcon style={{ fontSize: "13px", color: "#FAB610", margin: "auto 0px" }} />
                                                        <StarIcon style={{ fontSize: "13px", color: "#FAB610", margin: "auto 0px" }} />
                                                        <StarIcon style={{ fontSize: "13px", color: "#FAB610", margin: "auto 0px" }} />
                                                        <StarIcon style={{ fontSize: "13px", color: "#FAB610", margin: "auto 0px" }} />
                                                    </div>
                                                </Grid>
                                                <Grid item xl={12} lg={12} md={12} sm={12} xs={12} style={{ height: "40%", padding: "0", margin: "auto 0px 0px" }}>
                                                    <div style={{ marginRight: "0px", marginLeft: "auto", display: "flex", justifyContent: "flex-end", padding: "0px", height: "100%" }}>
                                                        <ShoppingCartIcon style={{ fontSize: "23px", color: "#C7C6C6", margin: "-10px 0px 0px", padding: "0px" }} />
                                                    </div>
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                </CardContent>
                            </Card>
                        </Grid>
                    }) : null}
                    <Stack style={{ margin: "30px auto auto" }} spacing={2}>
                        <Pagination onChange={(event, value) => {
                            setPage(value);
                        }} count={totalPage} />
                    </Stack>
                </Grid>
            </Grid>
        </Grid>
    );
};

export default ProductList;