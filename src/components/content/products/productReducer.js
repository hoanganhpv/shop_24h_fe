import {
    GET_ALL_PRODUCTS_REQUEST,
    GET_ALL_PRODUCTS_SUCCESS,
    GET_ALL_PRODUCTS_ERROR,
    GET_ALL_PRODUCTS_WITH_CONDITION_REQUEST,
    GET_ALL_PRODUCTS_WITH_CONDITION_SUCCESS,
    GET_ALL_PRODUCTS_WITH_CONDITION_ERROR
} from "./action/productsActionTypes";

const initialState = {
    allProducts: null,
    loading: false
};

const ProductReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_ALL_PRODUCTS_REQUEST: {
            return {
                ...state,
                loading: true
            };
        };
        case GET_ALL_PRODUCTS_SUCCESS: {
            return {
                ...state,
                allProducts: action.payload,
                loading: false
            };
        };
        case GET_ALL_PRODUCTS_ERROR: {
            return {
                ...state,
                allProducts: null,
                loading: false
            }
        }
        case GET_ALL_PRODUCTS_WITH_CONDITION_REQUEST: {
            return {
                ...state,
                loading: true
            };
        };
        case GET_ALL_PRODUCTS_WITH_CONDITION_SUCCESS: {
            return {
                ...state,
                allProducts: action.payload,
                loading: false
            };
        };
        case GET_ALL_PRODUCTS_WITH_CONDITION_ERROR: {
            return {
                ...state,
                allProducts: null,
                loading: false
            }
        }
        default: {
            return state
        };
    };
};


export default ProductReducer;