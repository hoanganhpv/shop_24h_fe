import { Grid, Typography } from "@mui/material";
import StarIcon from '@mui/icons-material/Star';
import { Button, ButtonGroup } from '@mui/material';
import detaialImg from "../../../assets/images/products/pottedPlants.jpg";
import detaialImg1 from "../../../assets/images/products/smallTable.jpg";
import detaialImg2 from "../../../assets/images/products/lamp.jpg";
import detaialImg3 from "../../../assets/images/products/modernChair.jpg";
import detaialImg4 from "../../../assets/images/products/metallicChair.jpg";
import * as React from 'react';

const Detail = () => {

    const [quantity, setQuantity] = React.useState(1);

    const handleQuantityChange = (newQuantity) => {
        setQuantity(newQuantity);
    };

    return (
        <>
            <Grid style={{ padding: "0% 2% 0% 2%", margin: "auto" }} item xl={9} lg={9} md={9} sm={9} xs={9}>
                {/* TOP - head */}
                <Grid item style={{ height: "8%", margin: "2%", maxHeight: "200px" }}>
                    <Grid container style={{ position: "relative", height: "100%" }}>

                    </Grid>
                </Grid>
                {/* BODY */}
                <Grid item style={{ height: "auto" }}>
                    <Grid container style={{ paddingBottom: "300px", margin: "auto" }}>
                        <Grid item xl={6} lg={6} md={6} sm={12} xs={12}>
                            <img src={detaialImg} width={"100%"}></img>
                            <Grid container style={{ paddingTop: "10%" }}>
                                <Grid item xl={3} lg={3} md={3} sm={3} xs={3} style={{ padding: "3px" }}>
                                    <img src={detaialImg1} width={"100%"} style={{ padding: "0", margin: "0" }}></img>
                                </Grid>
                                <Grid item xl={3} lg={3} md={3} sm={3} xs={3} style={{ padding: "3px" }}>
                                    <img src={detaialImg2} width={"100%"} style={{ padding: "0", margin: "0" }}></img>
                                </Grid>
                                <Grid item xl={3} lg={3} md={3} sm={3} xs={3} style={{ padding: "3px" }}>
                                    <img src={detaialImg3} width={"100%"} style={{ padding: "0", margin: "0" }}></img>
                                </Grid>
                                <Grid item xl={3} lg={3} md={3} sm={3} xs={3} style={{ padding: "3px" }}>
                                    <img src={detaialImg4} width={"100%"} style={{ padding: "0", margin: "0" }}></img>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid style={{ padding: "0px 20px 0px" }} item xl={5} lg={5} md={5} sm={12} xs={12}>
                            <Grid container>

                                <Grid item xl={12} lg={12} md={12} sm={12} xs={12}>
                                    <Grid container style={{ borderTop: "3.5px solid #FAB610", width: "15%" }}>
                                        <Grid item xl={12} lg={12} md={12} sm={12} xs={12} style={{ marginTop: "5%" }}>
                                            <Typography style={{ color: "#FAB610", fontSize: "23px", fontWeight: "600", lineHeight: "30px" }}>$180</Typography>
                                        </Grid>
                                    </Grid>
                                    <Grid container>
                                        <Grid item xl={12} lg={12} md={12} sm={12} xs={12} style={{ padding: "10% 0px", margin: "0" }}>
                                            <Typography style={{ fontSize: "30px", fontWeight: "500" }}>White Modern Furniture</Typography>
                                        </Grid>
                                        <Grid item xl={6} lg={6} md={6} sm={6} xs={6} style={{ padding: "0", margin: "0", paddingRight: "34%" }}>
                                            <div style={{ marginRight: "auto", marginLeft: "0px", display: "flex", justifyContent: "flex-end", padding: "0px", height: "100%" }}>
                                                <StarIcon style={{ fontSize: "13px", color: "#FAB610", margin: "auto 0px" }} />
                                                <StarIcon style={{ fontSize: "13px", color: "#FAB610", margin: "auto 0px" }} />
                                                <StarIcon style={{ fontSize: "13px", color: "#FAB610", margin: "auto 0px" }} />
                                                <StarIcon style={{ fontSize: "13px", color: "#FAB610", margin: "auto 0px" }} />
                                                <StarIcon style={{ fontSize: "13px", color: "#FAB610", margin: "auto 0px" }} />
                                            </div>
                                        </Grid>

                                        <Grid item xl={6} lg={6} md={6} sm={6} xs={6} style={{ padding: "0px 0px 0px 0px", margin: "0" }}>
                                            <div style={{ marginRight: "0px", marginLeft: "auto", display: "flex", justifyContent: "flex-end", padding: "0px", height: "100%" }}>
                                                <Typography >Write a review</Typography>
                                            </div>
                                        </Grid>

                                        <Grid item xl={12} lg={12} md={12} sm={12} xs={12} style={{ padding: "10% 0px", margin: "0" }}>
                                            <Typography>
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid quae eveniet culpa officia quidem mollitia impedit iste asperiores nisi reprehenderit consequatur, autem, nostrum pariatur enim?
                                            </Typography>
                                        </Grid>

                                        <Grid item xl={12} lg={12} md={12} sm={12} xs={12} style={{ padding: "10% 0px", margin: "auto" }}>
                                            <ButtonGroup style={{margin: "auto"}}>
                                                <Button onClick={() => handleQuantityChange(quantity - 1)} disabled={quantity === 1}>-</Button>
                                                <Button disabled>{quantity}</Button>
                                                <Button onClick={() => handleQuantityChange(quantity + 1)}>+</Button>
                                            </ButtonGroup>
                                        </Grid>

                                    </Grid>
                                </Grid>









                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </>
    );
};

export default Detail;