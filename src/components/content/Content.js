import { Grid } from "@mui/material";
import HomePage from "./homePage/HomePage.js";

const Content = () => {
    return (
        <Grid item xl={9} lg={9} md={12} sm={12} xs={12} style={{height: "100%"}}>
            <HomePage />
        </Grid>
    );
};

export default Content;