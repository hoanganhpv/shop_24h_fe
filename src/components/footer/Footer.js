import SaleFooter from "./saleFooter/SaleFooter.js";
import ContactFooter from "./contactFooter/ContactFooter.js";
import { Grid } from "@mui/material";

const Footer = () => {
    return (
        <Grid container justifyContent={"center"} style={{margin: "0px"}}>
            <Grid style={{ padding: "0", margin: "0px" }} item xl={12} lg={12} md={12} sm={12} xs={12}>
                <SaleFooter />
                <ContactFooter />
            </Grid>
        </Grid>
    );
};

export default Footer;