import { configureStore } from "@reduxjs/toolkit";
import thunk from "redux-thunk";
import ProductReducer from "./components/content/products/productReducer.js";

const store = configureStore({
    reducer: {
        products: ProductReducer
    },
    middleware: [thunk] 
});

export default store;